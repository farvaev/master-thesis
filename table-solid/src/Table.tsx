import {Component, createSignal, For, onCleanup, JSX, onMount} from "solid-js";
import {getColumnCode} from "../../table-react/src/common/getColumnCode";
import {getTableCellMemo, getTableCellStyles} from "./cellStates";
import {
	SelectedCell,
	Selection,
	registerSelectionEventListeners,
	unregisterSelectionEventListeners
} from "./selection";
import {registerEditingEventListeners, unregisterEditingEventListeners} from "./editing";
import {Toolbar} from "./toolbar";

const [rows, setRows] = createSignal(10);
const [columns, setColumns] = createSignal(10);

export const Table: Component = () => {
	let tableElem: any;
	onMount(() => {
		if (tableElem) registerEditingEventListeners(tableElem);
		if (tableElem) registerSelectionEventListeners(tableElem);
	});
	onCleanup(() => {
		if (tableElem) unregisterEditingEventListeners(tableElem);
		if (tableElem) unregisterSelectionEventListeners(tableElem);
	});

	return <div>
		<Toolbar/>
		<div
			class={"table-container"}
		>
			<div class="table">
					{/*<div*/}
					{/*	class={"table-selection-overlay"}*/}
					{/*	ref={tableElem}*/}
					{/*	tabIndex={"0"}*/}
					{/*/>*/}
				<table
					ref={tableElem}
					tabIndex={"0"}
				>
					<tbody>
					<For each={new Array(rows() + 1).fill(0)}>
						{
							(_, rowIndex) => {
								return <tr>
									<For each={new Array(columns() + 1).fill(0)}>
										{
											(_, colIndex) => {
												return <Td
													row={rowIndex()}
													col={colIndex()}
												/>;
											}
										}
									</For>
								</tr>
							}
						}
					</For>
					</tbody>
				</table>
				<Selection/>
				<SelectedCell/>
			</div>
			<div>&nbsp;&nbsp;&nbsp;</div>
			<AdditionForm
				onAdd={(additionalColumns) => setColumns(columns() + additionalColumns)}
				submitText={"Добавить столбцы"}
			/>
		</div>
		<div>&nbsp</div>
		<AdditionForm
			onAdd={(additionalRows) => setRows(rows() + additionalRows)}
			submitText={"Добавить строки"}
		/>
	</div>;
};

type TAdditionFormProps = {
	onAdd: (amount: number) => void,
	submitText: string,
};

const AdditionForm: Component<TAdditionFormProps> = (
	{
		onAdd,
		submitText,
	}
) => {
	const [val, setVal] = createSignal(0);

	return <form
		onSubmit={(e) => {
			e.preventDefault();
			onAdd(val());
		}}
	>
		<input
			class={"input"}
			type={"number"}
			value={val()}
			onChange={(e) => {
				setVal(parseInt((e.target as HTMLInputElement).value));
			}}
		/>
		<button type={"submit"}>
			{submitText}
		</button>
	</form>;
};

type TTdProps = {
	col: number,
	row: number,
};

const Td: Component<TTdProps> = (props) => {
	let cellCode: string | undefined = `${getColumnCode(props.col)}${props.row}`;

	const cellValue = getTableCellMemo(cellCode);

	let content: JSX.Element;

	if (props.row === 0) content = getColumnCode(props.col);
	if (props.col === 0) content = props.row;
	if (props.col === 0 && props.row === 0) content = <span/>;

	if (props.col === 0 || props.row === 0) {
		cellCode = undefined;
	}

	return <td
		class={props.col === 0 || props.row === 0 ? "border" : ""}
		style={(() => {
			if (!cellCode) return undefined;
			return getTableCellStyles(cellCode)[0]();
		})()}
	>
		{
			content ?
				content :
				<div
					data-id={cellCode}
					class={"td"}
				>
					<span>
						{cellValue()}
					</span>
				</div>
		}
	</td>;
};
