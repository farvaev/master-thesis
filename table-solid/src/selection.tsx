import {Component, createSignal, onCleanup, onMount} from "solid-js";
import {getTableCellSignal, getTableCellStyles} from "./cellStates";

export const [selection, setSelection] = createSignal<{
	topLeft: string,
	bottomRight: string,
	origin: string,
} | null>(null);

export const startSelecting = (cellCode: string) => {
	setSelection({
		topLeft: cellCode,
		bottomRight: cellCode,
		origin: cellCode,
	});
};
export const continueSelecting = (cellCode: string) => {
	const selectionOrigin = selection()?.origin;
	if (!selectionOrigin) return;
	const originCol = (selectionOrigin.match(/[A-z]+/) || [])[0];
	const originRow = (selectionOrigin.match(/\d+/) || [])[0];
	const cornerCol = (cellCode.match(/[A-z]+/) || [])[0];
	const cornerRow = (cellCode.match(/\d+/) || [])[0];
	let top: string;
	let bottom: string;
	if (parseInt(originCol) > parseInt(cornerCol)) {
		top = cornerCol;
		bottom = originCol;
	} else {
		top = originCol;
		bottom = cornerCol;
	}
	let left: string;
	let right: string;
	if (originRow.length > cornerRow.length || originRow > cornerRow) {
		left = cornerRow;
		right = originRow;
	} else {
		left = originRow;
		right = cornerRow;
	}
	setSelection({
		topLeft: `${left}${top}`,
		bottomRight: `${right}${bottom}`,
		origin: selectionOrigin,
	});
};

let trackSelection = false;
const onMouseDown = (e: MouseEvent) => {
	e.stopPropagation();
	// if (document.activeElement?.tagName === "TEXTAREA") return;
	setTimeout(() => {
		const x = e.x;
		const y = e.y;
		const cell = document
			.elementsFromPoint(x, y)
			.find((elem) => Boolean((elem as HTMLElement).dataset.id)) as HTMLElement | undefined;

		trackSelection = Boolean(cell);
		if (cell) {
			startSelecting(cell.dataset.id as string);
		}
	}, 1);
};

const onMouseMove = (e: MouseEvent) => {
	e.stopPropagation();
	if (!trackSelection) return;
	// const target = (e.target as HTMLElement);
	const x = e.x;
	const y = e.y;
	const cell = document
		.elementsFromPoint(x, y)
		.find((elem) => Boolean((elem as HTMLElement).dataset.id)) as HTMLElement | undefined;
	// const cell = getAncestor((e.target as HTMLElement), (el) => Boolean(el.dataset.id));
	if (!cell?.dataset.id) return;
	continueSelecting(cell.dataset.id);
};

const onMouseLeave = (e: MouseEvent) => {
	e.stopPropagation();
	trackSelection = false;
};

export const registerSelectionEventListeners = (elem: HTMLElement) => {
	elem?.addEventListener("mousedown", onMouseDown);
	elem?.addEventListener("mousemove", onMouseMove, { capture: true, });
	elem?.addEventListener("mouseup", onMouseLeave);
};

export const unregisterSelectionEventListeners = (elem: HTMLElement) => {
	elem?.removeEventListener("mousedown", onMouseDown);
	elem?.removeEventListener("mousemove", onMouseMove, { capture: true, });
	elem?.removeEventListener("mouseup", onMouseLeave);
};

export const Selection: Component = () => {
	let selectionElem: any;
	onMount(() => {
		if (selectionElem) registerSelectionEventListeners(selectionElem);
	});
	onCleanup(() => {
		if (selectionElem) unregisterSelectionEventListeners(selectionElem);
	});

	const getTopLeftCoords = () => {
		const cellCode = selection()?.topLeft;
		if (!cellCode) return null;
		const cellElem = document.querySelector(`[data-id='${cellCode}']`) as HTMLElement | undefined;
		if (!cellElem) return null;
		return {
			top: cellElem.offsetTop,
			left: cellElem.offsetLeft,
		};
	};
	const getBottomRightCoords = () => {
		const cellCode = selection()?.bottomRight;
		if (!cellCode) return null;
		const cellElem = document.querySelector(`[data-id='${cellCode}']`) as HTMLElement | undefined;
		if (!cellElem) return null;
		return {
			bottom: cellElem.offsetTop + cellElem.offsetHeight,
			right: cellElem.offsetLeft + cellElem.offsetWidth,
		};
	};

	return <div
		class={"selection"}
		ref={selectionElem}
		style={(() => {
			if (selection()?.topLeft === selection()?.bottomRight) return;
			const topLeft = getTopLeftCoords();
			const bottomRight = getBottomRightCoords();

			const table = document.querySelector(".table");

			if (!table) return;
			if (!topLeft || !bottomRight) return;

			const width = typeof bottomRight.right !== "undefined" && typeof topLeft.left !== "undefined" ?
				bottomRight.right - topLeft.left :
				0;
			const height = typeof bottomRight.bottom !== "undefined" && typeof topLeft.top !== "undefined" ?
				bottomRight.bottom - topLeft.top :
				0;

			return {
				top: `${topLeft.top}px`,
				left: `${topLeft.left}px`,
				width: `${width}px`,
				height: `${height}px`,
			};
		})()}
	>
	</div>;
};

export const SelectedCell: Component = () => {
	let cellElem: any;
	onMount(() => {
		if (cellElem) registerSelectionEventListeners(cellElem);
	});
	onCleanup(() => {
		if (cellElem) unregisterSelectionEventListeners(cellElem);
	});

	const getOriginElement = () => {
		const cellCode = selection()?.origin;
		if (!cellCode) return null;
		const cellElem = document.querySelector(`[data-id='${cellCode}']`);
		if (!cellElem) return null;
		return cellElem as HTMLElement;
	};
	const getInputSignal = () => {
		const cellCode = selection()?.origin;
		if (!cellCode) return null;
		return getTableCellSignal(cellCode);
	};

	return <div
		class={"selected-cell td"}
		ref={cellElem}
		style={(() => {
			const originElement = getOriginElement();
			if (!originElement) {
				return {
					display: "none",
				}
			}
			return {
				top: `${originElement.offsetTop}px`,
				left: `${originElement.offsetLeft}px`,
				"min-width": `${originElement.offsetWidth}px`,
				"min-height": `${originElement.offsetHeight}px`,
			};
		})()}
	>
		<textarea
			style={(() => {
				const cellCode = selection()?.origin;
				if (!cellCode) return undefined;
				return getTableCellStyles(cellCode)[0]();
			})()}
			onKeyDown={(e) => {
				if (e.key === "Enter" && !e.shiftKey) (e.target as HTMLTextAreaElement).blur();
			}}
			onChange={(e) => {
				const signal = getInputSignal();
				if (!signal) return;
				const value = (e.target as HTMLTextAreaElement).value;
				signal[1](value);
			}}
			value={getInputSignal() ? (getInputSignal() as any)[0]() : ""}
		/>
	</div>;
};
