export type GetTableCellSignal = (cellCode: string) => [() => any, (value: any) => void];

export const isNonCircular = (formula: string, getTableCellSignal: GetTableCellSignal, metCells: Record<string, boolean> = {}) => {
	const cells = (formula.match(/([A-z]+\d+)/gi) || []).map((cc) => cc.toUpperCase());
	if (!formula.startsWith("=")) return true;

	for (let i = 0, l = cells.length; i < l; i++) {
		if (metCells[cells[i]]) return false;
		metCells[cells[i]] = true;
		const isOk = isNonCircular(getTableCellSignal(cells[i])[0](), getTableCellSignal, metCells);
		if (!isOk) return false;
		metCells[cells[i]] = false;
	}

	return true;
};
