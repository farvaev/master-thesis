import {createMemo, createSignal} from "solid-js";
import {isNonCircular} from "./isNonCircular";

const tableCellSignals: Record<string, [() => string, (arg: string) => any]> = {};

export const getTableCellSignal = (cellCode: string) => {
	const signal = tableCellSignals[cellCode] || createSignal("");
	tableCellSignals[cellCode] = signal;
	return signal;
};

const tableCellMemos: Record<string, () => any> = {};

export const getTableCellMemo = (cellCode: string) => {
	const memo = tableCellMemos[cellCode] || createMemo(() => {
		const [inputValue] = getTableCellSignal(cellCode);
		if (!inputValue().startsWith("=")) {
			return inputValue();
		}
		if (!isNonCircular(inputValue(), getTableCellSignal)) return "!Circular dependencies!";

		const cells = (inputValue().match(/([A-z]+\d+)/gi) || []);
		let result = inputValue();
		cells.forEach((cellCode) => {
			const getter = getTableCellMemo(cellCode.toUpperCase());
			result = result.replace(cellCode, getter());
		});
		try {
			return eval(result.replace(/^=/, ""));
		} catch (_e) {
			return `err${inputValue()}`;
		}
	});

	tableCellMemos[cellCode] = memo;
	return memo;
};

export type CellStyle = Record<string, string | undefined>;
const tableCellStyles: Record<string, [() => CellStyle, (arg: CellStyle | ((old: CellStyle) => CellStyle)) => any]> = {};

export const getTableCellStyles = (cellCode: string) => {
	const signal = tableCellStyles[cellCode] || createSignal<CellStyle>({});
	tableCellStyles[cellCode] = signal;
	return signal;
};
