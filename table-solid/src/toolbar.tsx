import {getColumnCode, getColumnIndex} from "../../table-react/src/common/getColumnCode";
import {CellStyle, getTableCellStyles} from "./cellStates";
import {selection} from "./selection";
import {batch, createSignal, For} from "solid-js";

const format = (newStyles: CellStyle) => {
	const slctn = selection();
	if (!slctn?.topLeft || !slctn?.bottomRight) return;
	const topLeft = slctn.topLeft;
	const bottomRight = slctn.bottomRight;
	const cellCodes: string[] = [];

	const col1 = getColumnIndex((topLeft.match(/[A-z]+/) || [])[0]);
	const col2 = getColumnIndex((bottomRight.match(/[A-z]+/) || [])[0]);
	const row1 = parseInt((topLeft.match(/\d+/) || [])[0]);
	const row2 = parseInt((bottomRight.match(/\d+/) || [])[0]);

	for (let i = col1; i <= col2; i++) {
		for (let j = row1; j <= row2; j++) {
			cellCodes.push(`${getColumnCode(i)}${j}`);
		}
	}

	batch(() => {
		cellCodes.forEach((cellCode) => {
			const [, setStyles] = getTableCellStyles(cellCode);

			setStyles((old) => {
				return {
					...old,
					...newStyles,
				};
			});
		});
	});
};

const colors = [
	"#f44336",
	"#9c27b0",
	"#3f51b5",
	"#4caf50",
	"#ffeb3b",
	"#9e9e9e",
	"#000",
	"#fff",
];

export const Toolbar = () => {
	const [selectingColor, setSelectingColor] = createSignal<"bg" | "text" | null>(null);

	return <div
		class={"toolbar"}
	>
		<button
			class={"toolbar-button"}
			onClick={() => format({
				"text-align": "left",
			})}
		>
			<img class={"icon"} src={"src/icons/text-align-left.svg"} alt={""} />
		</button>

		<button
			class={"toolbar-button"}
			onClick={() => format({
				"text-align": "center",
			})}
		>
			<img class={"icon"} src={"src/icons/text-align-center.svg"} alt={""} />
		</button>

		<button
			class={"toolbar-button"}
			onClick={() => format({
				"text-align": "right",
			})}
		>
			<img class={"icon"} src={"src/icons/text-align-right.svg"} alt={""} />
		</button>

		<button
			class={"toolbar-button"}
			onClick={() => format({
				"text-align": "justify",
			})}
		>
			<img class={"icon"} src={"src/icons/text-align-justify.svg"} alt={""} />
		</button>

		<button
			class={"toolbar-button"}
			onClick={() => setSelectingColor("text")}
		>
			<img class={"icon"} src={"src/icons/text-color.svg"} alt={""} />
		</button>

		<button
			class={"toolbar-button"}
			onClick={() => setSelectingColor("bg")}
		>
			<img class={"icon"} src={"src/icons/bg-color.svg"} alt={""} />
		</button>

		<div
			class={"backdrop"}
			style={{
				display: selectingColor() !== null ? "flex" : "none",
			}}
			onClick={() => setSelectingColor(null)}
		>
			<div
				style={{
					background: "white",
					padding: "10px",
				}}
			>
				<For each={colors}>
					{
						(item) => {
							return <button
								class={"color-button"}
								style={{"background-color": item}}
								onClick={() => {
									format({
										[selectingColor() === "text" ? "color" : "background-color"]: item,
									});
									setSelectingColor(null);
								}}
							/>
						}
					}
				</For>
			</div>
		</div>
	</div>;
};
