/* @refresh reload */
import { render } from "solid-js/web";

import "../../table-react/src/common/index.css";
import App from "./App";

render(() => <App />, document.getElementById("root") as HTMLElement);
