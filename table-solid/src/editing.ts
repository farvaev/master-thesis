import {batch, createSignal} from "solid-js";
import {getColumnCode, getColumnIndex} from "../../table-react/src/common/getColumnCode";
import {selection, setSelection} from "./selection";
import {getTableCellSignal} from "./cellStates";

const [clipboard, setClipboard] = createSignal("");

export const handleCopy = (e: KeyboardEvent) => {
	if (!e.ctrlKey) return;
	if (e.key.toLowerCase() === "c") {
		const slctn = selection();
		if (!slctn?.topLeft || !slctn?.bottomRight) return;
		const topLeft = slctn.topLeft;
		const bottomRight = slctn.bottomRight;

		const col1 = getColumnIndex((topLeft.match(/[A-z]+/) || [])[0]);
		const col2 = getColumnIndex((bottomRight.match(/[A-z]+/) || [])[0]);
		const row1 = parseInt((topLeft.match(/\d+/) || [])[0]);
		const row2 = parseInt((bottomRight.match(/\d+/) || [])[0]);

		let clipboardValue = "";
		for (let i = col1; i <= col2; i++) {
			for (let j = row1; j <= row2; j++) {
				const cellCode = `${getColumnCode(i)}${j}`;
				clipboardValue += `${encodeURIComponent(getTableCellSignal(cellCode)[0]())},`;
			}
			clipboardValue += "\n";
		}
		setClipboard(clipboardValue);
	}
};

export const handlePaste = (e: KeyboardEvent) => {
	if (!e.ctrlKey) return;
	if (e.key.toLowerCase() === "v") {
		const slctn = selection();
		if (!slctn?.origin) return;
		const origin = slctn.origin;

		const col1 = getColumnIndex((origin.match(/[A-z]+/) || [])[0]);
		const row1 = parseInt((origin.match(/\d+/) || [])[0]);

		let clipboardValues = clipboard().split(",\n").map((row) => row.split(","));
		clipboardValues.pop(); // remove empty col

		const sl = {
			topLeft: origin,
			bottomRight: origin,
			origin,
		};
		batch(() => {
			clipboardValues.forEach((col, colIndex) => {
				col.forEach((item, rowIndex) => {
					const cellCode = `${getColumnCode(col1 + colIndex)}${row1 + rowIndex}`;
					getTableCellSignal(cellCode)[1](decodeURIComponent(item));
					sl.bottomRight = cellCode;
				});
			});
			setSelection(sl);
		});
	}
};

export const registerEditingEventListeners = (elem: HTMLElement) => {
	elem?.addEventListener("keydown", focusOnTextareaOnTyping);
	elem?.addEventListener("keydown", handleCopy);
	elem?.addEventListener("keydown", handlePaste);
};

export const unregisterEditingEventListeners = (elem: HTMLElement) => {
	elem?.removeEventListener("keydown", focusOnTextareaOnTyping);
	elem?.removeEventListener("keydown", handleCopy);
	elem?.removeEventListener("keydown", handlePaste);
};

const focusOnTextareaOnTyping = (e: KeyboardEvent) => {
	e.stopPropagation();
	if (e.ctrlKey) return;
	document.querySelector("textarea")?.focus();
};
