import type {Component} from "solid-js";
import {Table} from "./Table";

const App: Component = () => {
	return <div>
		<Table/>
	</div>;
};

export default App;
