import {getColumnCode} from "../../table-react/src/common/getColumnCode";

let rows = 100;
let cols = 10;

export const initTable = (elem: HTMLElement) => {
	elem.innerHTML = `
<div>
<div
	class="table-container"
>
<table>
<tbody>
	${new Array(rows + 1).fill(0).map((_, row) => {
		return `<tr>${new Array(cols + 1).fill(0).map((_, col) => {
			let cellCode: string | undefined = `${row}${getColumnCode(col)}`;
			let content: string = "";
			if (row === 0) content = getColumnCode(col);
			if (col === 0) content = row + "";
			if (col === 0 && row === 0) content = "<span></span>";
			return `
			<td 
				class="${(col === 0 || row === 0) ? "border" : ""}"
				data-id="${cellCode}">${
						content ?
							content :
							`<div class="td">
								<span></span>
								<textarea 
									data-textarea="${cellCode}"
									placeholder="${cellCode}"
								></textarea>
							</div>`
					}
			</td>`;
		}).join("")}</tr>`;
	}).join("")}
</tbody>
</table>
</div>
</div>
	`.replace(/(\t+|\n+)/g, "");

	elem.addEventListener("blur", (e) => {
		const target = (e.target as HTMLTextAreaElement);
		const val = target.value;
		const cell = target.parentElement?.querySelector("span");
		if (cell) cell.innerHTML = val;
	}, { capture: true, });
};

const data: Record<string, {
	value: string,
	displayValue: string,
}> = {
};

const setCellValue = (cellCode: string, value: string) => {
	const cell = document.querySelector(`[data-id="${cellCode}"]`);
	const displayValue =
};

// <textarea
// 	onKeyDown={(e) => {
// 	if (e.key === "Enter" && !e.shiftKey) (e.target as HTMLTextAreaElement).blur();
// }}
// onChange={(e) => {
// 	const value = e.target.value;
// 	setInputValue(value);
// }}
// value={inputValue}
// placeholder={cellCode}
// />
