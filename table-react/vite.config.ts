import {defineConfig} from "vite";
import react from "@vitejs/plugin-react";

export default defineConfig({
	plugins: [react()],
	preview: {
		port: 3003,
	},
	build: {
		outDir: "../public",
	},
});
