import React, {useState} from "react";
import {getColumnCode, getColumnIndex} from "./common/getColumnCode";
import {CellStyle, getStyleStore} from "./cellState";
import {selectionStore} from "./selection";

const colors = [
	"#f44336",
	"#9c27b0",
	"#3f51b5",
	"#4caf50",
	"#ffeb3b",
	"#9e9e9e",
	"#000",
	"#fff",
];

export const Toolbar = () => {
	const [selectingColor, setSelectingColor] = useState<"bg" | "text" | null>(null);

	const format = (newStyles: CellStyle) => {
		const slctn = selectionStore.getState();
		if (!slctn?.topLeft || !slctn?.bottomRight) return;
		const topLeft = slctn.topLeft;
		const bottomRight = slctn.bottomRight;
		const cellCodes: string[] = [];

		const col1 = getColumnIndex((topLeft.match(/[A-z]+/) || [])[0]);
		const col2 = getColumnIndex((bottomRight.match(/[A-z]+/) || [])[0]);
		const row1 = parseInt((topLeft.match(/\d+/) || [])[0]);
		const row2 = parseInt((bottomRight.match(/\d+/) || [])[0]);

		for (let i = col1; i <= col2; i++) {
			for (let j = row1; j <= row2; j++) {
				cellCodes.push(`${getColumnCode(i)}${j}`);
			}
		}

		(() => {
			cellCodes.forEach((cellCode) => {
				const {setState} = getStyleStore(cellCode);

				setState((old) => {
					return {
						...old,
						...newStyles,
					};
				});
			});
		})();
	};

	return <div
		className={"toolbar"}
	>
		<button
			className={"toolbar-button"}
			onClick={() => format({
				"text-align": "left",
			})}
		>
			<img className={"icon"} src={"icons/text-align-left.svg"} alt={""} />
		</button>

		<button
			className={"toolbar-button"}
			onClick={() => format({
				"text-align": "center",
			})}
		>
			<img className={"icon"} src={"icons/text-align-center.svg"} alt={""} />
		</button>

		<button
			className={"toolbar-button"}
			onClick={() => format({
				"text-align": "right",
			})}
		>
			<img className={"icon"} src={"icons/text-align-right.svg"} alt={""} />
		</button>

		<button
			className={"toolbar-button"}
			onClick={() => format({
				"text-align": "justify",
			})}
		>
			<img className={"icon"} src={"icons/text-align-justify.svg"} alt={""} />
		</button>

		<button
			className={"toolbar-button"}
			onClick={() => setSelectingColor("text")}
		>
			<img className={"icon"} src={"icons/text-color.svg"} alt={""} />
		</button>

		<button
			className={"toolbar-button"}
			onClick={() => setSelectingColor("bg")}
		>
			<img className={"icon"} src={"icons/bg-color.svg"} alt={""} />
		</button>

		<div
			className={"backdrop"}
			style={{
				display: selectingColor !== null ? "flex" : "none",
			}}
			onClick={() => setSelectingColor(null)}
		>
			<div
				style={{
					background: "white",
					padding: "10px",
				}}
			>
				{
					colors.map((item) => {
						return <button
							key={item}
							className={"color-button"}
							style={{backgroundColor: item}}
							onClick={() => {
								format({
									[selectingColor === "text" ? "color" : "backgroundColor"]: item,
								});
								setSelectingColor(null);
							}}
						/>
					})
				}
			</div>
		</div>
	</div>;
};
