import React, {useEffect, useMemo, useState} from "react";

import { VariableSizeGrid } from "react-window";

import {getColumnCode} from "./common/getColumnCode";
import {useAtomValue} from "jotai";
import {cellStyleAtoms, cellValueAtoms} from "./cellState";
import {Toolbar} from "./toolbar";
import {
	registerSelectionEventListeners,
	SelectedCell,
	Selection,
	unregisterSelectionEventListeners
} from "./selection";
import {registerEditingEventListeners, unregisterEditingEventListeners} from "./editing";
import {GridWithStickyCells} from "./VarSizeGrid";

export const getColWidth = (_a: number) => 140;
export const getRowHeight = (_a: number) => 28;

const Table = () => {
	const [rows, setRows] = useState(100);
	const [columns, setColumns] = useState(100);

	const { width, height, } = useMemo(() => {
		const maxWidth = window.innerWidth - 240;
		const maxHeight = window.innerHeight - 120;
		const width = Math.min(maxWidth, getColWidth(1) * columns);
		const height = Math.min(maxHeight, getRowHeight(1) * rows);
		return {
			width,
			height,
		}
	}, [rows, columns]);

	const [tableElem, setTableElem] = useState<HTMLDivElement>();
	useEffect(() => {
		if (tableElem) tableElem.style.userSelect = "none";
		if (tableElem) registerSelectionEventListeners(tableElem);
		return () => {
			if (tableElem) unregisterSelectionEventListeners(tableElem);
		};
	}, [tableElem]);
	useEffect(() => {
		if (tableElem) tableElem.setAttribute("tabindex", "0");
		if (tableElem) tableElem.style.outline = "none";
		if (tableElem) registerEditingEventListeners(tableElem);
		return () => {
			if (tableElem) unregisterEditingEventListeners(tableElem);
		};
	}, [tableElem]);

	return <div>
		<Toolbar/>
		<div
			className={"table-container"}
		>
			<div
				className={"table"}
			>
				<GridWithStickyCells
					columnWidth={getColWidth}
					rowHeight={getRowHeight}
					columnCount={columns}
					height={height}
					rowCount={rows}
					width={width}
					innerElementType={"div"}
					outerTagName={"div"}
					innerRef={(node: HTMLDivElement) => {
						if (!node) return;
						setTableElem(node);
					}}
					overscanColumnCount={2}
					overscanRowCount={5}
					estimatedColumnWidth={getColWidth(1)}
					estimatedRowHeight={getRowHeight(1)}
					useIsScrolling
				>
					{Cell}
				</GridWithStickyCells>
				<Selection portal={tableElem} />
				<SelectedCell portal={tableElem} />
			</div>
			<AdditionForm
				onAdd={(additionalColumns) => setColumns(columns + additionalColumns)}
				submitText={"Добавить столбцы"}
			/>
		</div>
		<AdditionForm
			onAdd={(additionalRows) => setRows(rows + additionalRows)}
			submitText={"Добавить строки"}
		/>
	</div>;
};

type TAdditionFormProps = {
	onAdd: (amount: number) => void,
	submitText: string,
};

const AdditionForm: React.FC<TAdditionFormProps> = (
	{
		onAdd,
		submitText,
	}
) => {
	const [val, setVal] = useState(0);

	return <form
		style={{
			margin: 10,
		}}
		onSubmit={(e) => {
			e.preventDefault();
			onAdd(val);
		}}
	>
		<input
			style={{
				width: 100,
				marginRight: 10,
				marginBottom: 10,
				display: "inline-block",
			}}
			type={"number"}
			value={val}
			onChange={(e) => {
				setVal(parseInt(e.target.value));
			}}
		/>
		<button type={"submit"}>
			{submitText}
		</button>
	</form>;
};

const Cell: React.FC<{
	columnIndex: number,
	rowIndex: number,
	style: any,
	isScrolling: boolean,
}> = React.memo((
	{
		columnIndex,
		rowIndex,
		style,
		// isScrolling,
	}
) => {
	const colCode = getColumnCode(columnIndex);
	const cellCode = `${colCode}${rowIndex}`;

	// if (isScrolling) {
	// 	return <div
	// 		style={style}
	// 		data-id={columnIndex === 0 || rowIndex === 0 ? undefined : cellCode}
	// 	>
	// 		<div className={"td-wrapper"}/>
	// 	</div>;
	// }

	return <div
		style={style}
		data-id={columnIndex === 0 || rowIndex === 0 ? undefined : cellCode}
	>
		<Td
			row={rowIndex}
			col={columnIndex}
			border={columnIndex === 0 || rowIndex === 0}
			cellCode={cellCode}
			colCode={colCode}
		/>
	</div>;
});

type TTdProps = {
	col: number,
	row: number,
	border: boolean,
	cellCode: string,
	colCode: string,
};

const Td: React.FC<TTdProps> = ({ col, row, border, cellCode, colCode, }) => {
	let content: React.ReactNode;

	if (row === 0) content = colCode;
	if (col === 0) content = row;
	if (col === 0 && row === 0) content = <span/>;

	const valueAtom = cellValueAtoms(cellCode);
	const cellValue = useAtomValue(valueAtom);

	const style = useAtomValue(cellStyleAtoms(cellCode));

	return <div
		className={(col === 0 || row === 0) ? "border td-wrapper" : "td-wrapper"}
		style={style}
	>
		{
			content ?
				content :
				<span
					className={"td"}
				>
					<span>
						{cellValue || ""}
					</span>
				</span>
		}
	</div>;
};

// const Yo = () => {
// 	useEffect(() => {
// 		const a = document.querySelector("[data-id='1A']") as HTMLTextAreaElement;
// 		const b = document.querySelector("[data-id='1B']") as HTMLTextAreaElement;
// 		const c = document.querySelector("[data-id='1C']") as HTMLTextAreaElement;
// 		a.value = "10";
// 		b.value = "20";
// 		c.value = "=1a+1b";
// 		a.focus();
// 		a.blur();
// 		b.focus();
// 		b.blur();
// 		c.focus();
// 		c.blur();
// 	}, []);
// 	return null;
// };

export default Table;
