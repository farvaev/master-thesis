export const evalFormula = (formula: string) => {
	const arr = formula.replace(/ +/g, "").split(/(?=[()+\-/*])|(?<=[()+\-/*])/g);
	return infToPost(arr);
	// return eval(formula);
};

const getPrecedence = (c: string) => {
	if (c == "^") {
		return 3;
	}
	else if (c == "/" || c == "*") {
		return 2;
	}
	else if (c == "+" || c == "-") {
		return 1;
	}
	else {
		return 0;
	}
};

const isOperator = (c: string) => {
	return c === "+" || c === "-" || c === "*" || c === "/";
};

const infToPost = (arr: string[]) => {
	const postfix: string[] = [];
	const stack: string[] = [];

	arr.forEach((elem, i) => {
		if (isOperator(elem)) {
			while (stack.length > 0 && getPrecedence(elem) <= getPrecedence(stack[stack.length - 1])) {
				postfix.push(stack.pop() as string);
			}
			stack.push(elem);
		}
		else if (elem === "(") {
			stack.push("(");
		}
		else if (elem === ")") {
			while(stack[stack.length - 1] != "(") {
				postfix.push(stack.pop() as string);
			}
			stack.pop();
		}
		else {
			postfix.push(elem);
		}
	});

	while (stack.length > 0) {
		postfix.push(stack.pop() as string);
	}

	let i = 0;
	// =1+2*3
	while (postfix.length > 1) {
		const el = postfix[i];
		if (isOperator(el)) {
			const el1 = postfix[i - 2];
			const el2 = postfix[i - 1];
			postfix.splice(i - 2, 3, calc(el1, el2, el)+"");
			i -= 1;
		} else {
			i += 1;
		}
	}

	return postfix[0];
};

const calc = (_el1: string, _el2: string, operator: string) => {
	const el1 = parseFloat(_el1);
	const el2 = parseFloat(_el2);

	switch (operator) {
		case "-":
			return el1 - el2;
		case "+":
			return el1 + el2;
		case "*":
			return el1 * el2;
		case "/":
			return el1 / el2;
	}
	return 0;
};
