import {atomFamily} from "jotai/utils";
import {Atom, atom, WritableAtom} from "jotai";
import {isNonCircular} from "./isNonCircular";
import createStore, {StoreApi} from "zustand/vanilla";
import {atomWithStore} from "jotai/zustand";
import {evalFormula} from "./evalFormula";

export type TInputValueState = { value: string };
const inputValueStores: Record<string, StoreApi<TInputValueState>> = {
};
const inputValueAtoms: Record<string, WritableAtom<TInputValueState, TInputValueState, any>> = {
};
export const getInputValueStore = (cellCode: string) => {
	const store = inputValueStores[cellCode] || createStore(() => ({}));
	const atom = inputValueAtoms[cellCode] || atomWithStore(store);
	inputValueStores[cellCode] = store;
	inputValueAtoms[cellCode] = atom;
	return store;
};
export const cellInputValueAtoms = (cellCode: string) => {
	const store = inputValueStores[cellCode] || createStore(() => ({}));
	const atom = inputValueAtoms[cellCode] || atomWithStore(store);
	inputValueStores[cellCode] = store;
	inputValueAtoms[cellCode] = atom;
	return atom;
};
export const cellInputValueAtomsBound = (cellCode: string) => {
	return inputValueAtoms[cellCode] as WritableAtom<TInputValueState, TInputValueState, any> | undefined;
};

// export const cellInputValueAtoms = ()

export const cellValueAtoms = atomFamily((cellCode: string) => atom((get) => {
	const inputValue = get(cellInputValueAtoms(cellCode))?.value || "";
	if (!inputValue.startsWith("=")) {
		return inputValue;
	}
	if (!isNonCircular(inputValue, get)) return "Circular deps :(";

	const cells = (inputValue.match(/([A-z]+\d+)/gi) || []);
	let result = inputValue;
	cells.forEach((cellCode) => {
		const getter = cellValueAtoms(cellCode.toUpperCase());
		result = result.replace(cellCode, `${getter ? get(getter) || "UNDEFINED" : cellCode || "UNDEFINED"}`);
	});
	try {
		const val = evalFormula(result.replace(/^=/, ""));
		if (val === "NaN") return `err${inputValue}`;
		return val;
	} catch (_e) {
		return `err${inputValue}`;
	}
}));


export type CellStyle = Record<string, string | undefined>;

const styleStores: Record<string, StoreApi<CellStyle>> = {
};
const styleAtoms: Record<string, Atom<CellStyle>> = {
};
export const getStyleStore = (cellCode: string) => {
	const store = styleStores[cellCode] || createStore(() => ({}));
	const atom = styleAtoms[cellCode] || atomWithStore(store);
	styleStores[cellCode] = store;
	styleAtoms[cellCode] = atom;
	return store;
};
export const cellStyleAtoms = (cellCode: string) => {
	const store = styleStores[cellCode] || createStore(() => ({}));
	const atom = styleAtoms[cellCode] || atomWithStore(store);
	styleStores[cellCode] = store;
	styleAtoms[cellCode] = atom;
	return atom;
};
