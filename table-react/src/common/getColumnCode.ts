export const getColumnCode = (num: number) => {
	let s = "";
	let t = 0;

	while (num > 0) {
		t = (num - 1) % 26;
		s = String.fromCharCode(65 + t) + s;
		num = (num - t)/26 | 0;
	}
	return s;
};

const alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];
export const getColumnIndex = (code: string) => {
	const splitted = code.split("");
	return splitted.reduce((result, char, index) => {
		return result + (alphabet.indexOf(char.toLowerCase())+1)*Math.pow(26, splitted.length - index - 1);
	}, 0);
};
