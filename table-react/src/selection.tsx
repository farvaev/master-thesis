import React, {useEffect, useMemo, useState} from "react";

import createStore from "zustand/vanilla";
import {atomWithStore} from "jotai/zustand";
import {useAtom, useAtomValue} from "jotai";
import {cellInputValueAtoms, cellStyleAtoms} from "./cellState";
import ReactDOM from "react-dom";
import {getColumnCode, getColumnIndex} from "./common/getColumnCode";
import {getColWidth, getRowHeight} from "./Table2";

function sumRowsHeights(index: number) {
	let sum = 0;

	while (index >= 1) {
		sum += getRowHeight(index - 1);
		index -= 1;
	}

	return sum;
}

function sumColumnWidths(index: number) {
	let sum = 0;

	while (index >= 1) {
		sum += getColWidth(index - 1);
		index -= 1;
	}

	return sum;
}

export type TSelection = {
	topLeft?: string | null,
	bottomRight?: string | null,
	origin?: string | null,
};
export const selectionStore = createStore<TSelection>(() => ({}));
export const selectionAtom = atomWithStore(selectionStore);

export const startSelecting = (cellCode: string) => {
	selectionStore.setState({
		topLeft: cellCode,
		bottomRight: cellCode,
		origin: cellCode,
	});
};
export const continueSelecting = (cellCode: string) => {
	const selectionOrigin = selectionStore.getState()?.origin;
	if (!selectionOrigin) return;
	const originCol = getColumnIndex((selectionOrigin.match(/[A-z]+/) || [])[0]);
	const originRow = parseInt((selectionOrigin.match(/\d+/) || [])[0]);
	const cornerCol = getColumnIndex((cellCode.match(/[A-z]+/) || [])[0]);
	const cornerRow = parseInt((cellCode.match(/\d+/) || [])[0]);
	let top: string;
	let bottom: string;
	if (originCol > cornerCol) {
		top = getColumnCode(cornerCol);
		bottom = getColumnCode(originCol);
	} else {
		top = getColumnCode(originCol);
		bottom = getColumnCode(cornerCol);
	}
	let left: string;
	let right: string;
	if (originRow > cornerRow) {
		left = cornerRow + "";
		right = originRow + "";
	} else {
		left = originRow + "";
		right = cornerRow + "";
	}
	selectionStore.setState({
		topLeft: `${left}${top}`,
		bottomRight: `${right}${bottom}`,
	});
};

let trackSelection = false;
const onMouseDown = (e: MouseEvent) => {
	e.stopPropagation();
	// if (document.activeElement?.tagName === "TEXTAREA") return;
	setTimeout(() => {
		const x = e.x;
		const y = e.y;
		const cell = document
			.elementsFromPoint(x, y)
			.find((elem) => Boolean((elem as HTMLElement).dataset.id)) as HTMLElement | undefined;

		trackSelection = Boolean(cell);
		if (cell) {
			if (e.shiftKey) {
				continueSelecting(cell.dataset.id as string);
			} else {
				startSelecting(cell.dataset.id as string);
			}
		}
	}, 1);
};

let lastMovedOver = "";
const onMouseMove = (e: MouseEvent) => {
	e.stopPropagation();
	if (!trackSelection) return;
	// const target = (e.target as HTMLElement);
	const x = e.x;
	const y = e.y;
	const cell = document
		.elementsFromPoint(x, y)
		.find((elem) => Boolean((elem as HTMLElement).dataset.id)) as HTMLElement | undefined;
	// const cell = getAncestor((e.target as HTMLElement), (el) => Boolean(el.dataset.id));
	if (!cell?.dataset.id) return;
	if (cell?.dataset.id === lastMovedOver) return;
	lastMovedOver = cell.dataset.id;
	continueSelecting(cell.dataset.id);
};

const onMouseLeave = (e: MouseEvent) => {
	e.stopPropagation();
	trackSelection = false;
};

export const registerSelectionEventListeners = (elem: HTMLElement) => {
	elem?.addEventListener("mousedown", onMouseDown);
	elem?.addEventListener("mousemove", onMouseMove, { capture: true, });
	elem?.addEventListener("mouseup", onMouseLeave);
};

export const unregisterSelectionEventListeners = (elem: HTMLElement) => {
	elem?.removeEventListener("mousedown", onMouseDown);
	elem?.removeEventListener("mousemove", onMouseMove, { capture: true, });
	elem?.removeEventListener("mouseup", onMouseLeave);
};

export const Selection: React.FC<{portal: HTMLDivElement | undefined}> = (
	{
		portal,
	}) => {
	const [selectionElem, setSelectionElem] = useState<HTMLDivElement>();
	useEffect(() => {
		if (selectionElem) registerSelectionEventListeners(selectionElem);
		return () => {
			if (selectionElem) unregisterSelectionEventListeners(selectionElem);
		};
	}, [selectionElem]);

	const selection = useAtomValue(selectionAtom);

	const getTopLeftCoords = () => {
		const cellCode = selection?.topLeft;
		if (!cellCode) return null;

		const col = getColumnIndex((cellCode.match(/[A-z]+/) || [])[0]);
		const row = parseInt((cellCode.match(/\d+/) || [])[0]);

		return {
			top: sumRowsHeights(row),
			left: sumColumnWidths(col),
		};
	};
	const getBottomRightCoords = () => {
		const cellCode = selection?.bottomRight;
		if (!cellCode) return null;

		const col = getColumnIndex((cellCode.match(/[A-z]+/) || [])[0]);
		const row = parseInt((cellCode.match(/\d+/) || [])[0]);

		return {
			bottom: sumRowsHeights(row + 1),
			right: sumColumnWidths(col + 1),
		};
	};

	const style = useMemo(() => {
		if (selection?.topLeft === selection?.bottomRight) return;
		const topLeft = getTopLeftCoords();
		const bottomRight = getBottomRightCoords();

		const table = document.querySelector(".table");

		if (!table) return;
		if (!topLeft || !bottomRight) return;

		const width = typeof bottomRight.right !== "undefined" && typeof topLeft.left !== "undefined" ?
			bottomRight.right - topLeft.left :
			0;
		const height = typeof bottomRight.bottom !== "undefined" && typeof topLeft.top !== "undefined" ?
			bottomRight.bottom - topLeft.top :
			0;

		return {
			top: `${topLeft.top - 1}px`,
			left: `${topLeft.left - 1}px`,
			width: `${width + 1}px`,
			height: `${height + 1}px`,
		};
	}, [selection]);

	if (!portal) return null;

	return ReactDOM.createPortal(<div
		className={"selection"}
		ref={(node) => setSelectionElem(node || undefined)}
		style={style}
	/>, portal);
};

export const SelectedCell: React.FC<{portal: HTMLDivElement | undefined}> = (
	{
		portal,
	}) => {
	const [selectionElem, setSelectionElem] = useState<HTMLDivElement>();
	useEffect(() => {
		if (selectionElem) registerSelectionEventListeners(selectionElem);
		return () => {
			if (selectionElem) unregisterSelectionEventListeners(selectionElem);
		};
	}, [selectionElem]);

	const selection = useAtomValue(selectionAtom);

	const [inputValue, setInputValue] = useAtom(cellInputValueAtoms(selection.origin || ""));
	const [_inputValue, _setInputValue] = useState(inputValue.value);
	useEffect(() => {
		_setInputValue(inputValue.value);
	}, [inputValue]);

	const style = useAtomValue(cellStyleAtoms(selection.origin || ""));

	const getCellParams = () => {
		const cellCode = selection?.origin;
		if (!cellCode) return null;

		const col = getColumnIndex((cellCode.match(/[A-z]+/) || [])[0]);
		const row = parseInt((cellCode.match(/\d+/) || [])[0]);

		return {
			top: sumRowsHeights(row) - 1,
			left: sumColumnWidths(col) - 1,
			width: getColWidth(col + 1) + 1,
			height: getRowHeight(row + 1) + 1,
		};
	};

	if (!portal) return null;

	return ReactDOM.createPortal(<div
		className={"selected-cell td"}
		ref={(node) => setSelectionElem(node || undefined)}
		style={(() => {
			const params = getCellParams();
			if (!params) {
				return {
					display: "none",
				}
			}
			return {
				top: params.top,
				left: params.left,
				minWidth: params.width,
				minHeight: params.height,
			};
		})()}
	>
		<textarea
			style={style}
			onKeyDownCapture={(e) => {
				if (e.ctrlKey) return;
				if (e.key === "Enter" && !e.shiftKey) {
					e.stopPropagation();
					setInputValue({ value: _inputValue });
					(e.target as HTMLTextAreaElement).blur();
				}
			}}
			onChange={(e) => {
				const value = (e.target as HTMLTextAreaElement).value;
				_setInputValue(value);
			}}
			onBlur={(e) => {
				e.stopPropagation();
				setInputValue({ value: _inputValue });
				portal?.focus();
			}}
			value={_inputValue || ""}
		/>
	</div>, portal);
};
