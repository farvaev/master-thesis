import {getColumnCode, getColumnIndex} from "./common/getColumnCode";
import {selectionStore} from "./selection";
import createStore from "zustand/vanilla";
import {getInputValueStore} from "./cellState";

export type TClipboard = { value: string };
export const clipboardStore = createStore<TClipboard>(() => ({ value: "" }));

export const handleCopy = (e: KeyboardEvent) => {
	if (!e.ctrlKey) return;
	if (e.key.toLowerCase() === "c") {
		const slctn = selectionStore.getState();
		if (!slctn?.topLeft || !slctn?.bottomRight) return;
		const topLeft = slctn.topLeft;
		const bottomRight = slctn.bottomRight;

		const col1 = getColumnIndex((topLeft.match(/[A-z]+/) || [])[0]);
		const col2 = getColumnIndex((bottomRight.match(/[A-z]+/) || [])[0]);
		const row1 = parseInt((topLeft.match(/\d+/) || [])[0]);
		const row2 = parseInt((bottomRight.match(/\d+/) || [])[0]);

		let clipboardValue = "";
		for (let i = col1; i <= col2; i++) {
			for (let j = row1; j <= row2; j++) {
				const cellCode = `${getColumnCode(i)}${j}`;
				const val = getInputValueStore(cellCode).getState().value;
				clipboardValue += `${val ? encodeURIComponent(val) : ""},`;
			}
			clipboardValue += "\n";
		}
		clipboardStore.setState({ value: clipboardValue });
	}
};

export const handlePaste = (e: KeyboardEvent) => {
	if (!e.ctrlKey) return;
	if (e.key.toLowerCase() === "v") {
		const slctn = selectionStore.getState();
		if (!slctn?.origin) return;
		const origin = slctn.origin;

		const col1 = getColumnIndex((origin.match(/[A-z]+/) || [])[0]);
		const row1 = parseInt((origin.match(/\d+/) || [])[0]);

		let clipboardValues = clipboardStore.getState().value.split(",\n").map((row) => row.split(","));
		clipboardValues.pop(); // remove empty col

		const sl = {
			topLeft: origin,
			bottomRight: origin,
			origin,
		};
		(() => {
			clipboardValues.forEach((col, colIndex) => {
				col.forEach((item, rowIndex) => {
					const cellCode = `${getColumnCode(col1 + colIndex)}${row1 + rowIndex}`;
					getInputValueStore(cellCode).setState({ value: decodeURIComponent(item) });
					sl.bottomRight = cellCode;
				});
			});
			selectionStore.setState(sl);
		})();
	}
};

export const registerEditingEventListeners = (elem: HTMLElement) => {
	elem?.addEventListener("keydown", focusOnTextareaOnTyping);
	elem?.addEventListener("keydown", handleCopy);
	elem?.addEventListener("keydown", handlePaste);
};

export const unregisterEditingEventListeners = (elem: HTMLElement) => {
	elem?.removeEventListener("keydown", focusOnTextareaOnTyping);
	elem?.removeEventListener("keydown", handleCopy);
	elem?.removeEventListener("keydown", handlePaste);
};

const focusOnTextareaOnTyping = (e: KeyboardEvent) => {
	e.stopPropagation();
	if (e.ctrlKey || e.shiftKey || e.altKey) return;
	document.querySelector("textarea")?.focus();
};
