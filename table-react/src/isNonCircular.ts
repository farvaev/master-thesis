import {cellInputValueAtomsBound} from "./cellState";

export const isNonCircular = (formula: string, get: (atom: any) => any, metCells: Record<string, boolean> = {}) => {
	const cells = (formula?.match(/(\d+[A-z]+)/gi) || []).map((cc) => cc.toUpperCase());
	if (!formula?.startsWith("=")) return true;

	for (let i = 0, l = cells.length; i < l; i++) {
		if (metCells[cells[i]]) return false;
		metCells[cells[i]] = true;
		const atom = cellInputValueAtomsBound(cells[i]);
		const isOk = isNonCircular(get(atom)?.value, get, metCells);
		if (!isOk) return false;
		metCells[cells[i]] = false;
	}

	return true;
};
